package io.github.dvegasa.usershowing.screens;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.dvegasa.usershowing.R;
import io.github.dvegasa.usershowing.helpers.RvUsersAdapter;
import io.github.dvegasa.usershowing.models.RandomUserResult;

public class UsersListActivity extends AppCompatActivity implements UsersListContract.View {
    private final String TAG = UsersListActivity.class.getName();

    @BindView(R.id.rvUsers)
    RecyclerView rvUsers;

    RvUsersAdapter adapter;

    UsersListContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);
        ButterKnife.bind(this);
        presenter = new UsersListPresenter();
        presenter.attachView(this);

        initRvUsers();
        presenter.viewIsReady();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroyView();
    }

    private void initRvUsers() {
        rvUsers.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void updateUsers(List<RandomUserResult.Result> userResultList) {
        adapter = new RvUsersAdapter(this);
        rvUsers.setAdapter(adapter);
        adapter.setList(userResultList);
    }

    @Override
    public void showErrorNotification(Throwable t) {
        Toast.makeText(this, "Something goes wrong", Toast.LENGTH_SHORT).show();
    }
}
