package io.github.dvegasa.usershowing.network;

import io.github.dvegasa.usershowing.screens.ModelStatusCallback;

/**
 * 11.07.2018
 */
public interface RetrofitHelperContract {

    public void prepare(ModelStatusCallback callback);

    public void downloadRandomUsers(int n, ModelStatusCallback.onFinishedRandomUsersResult callback);
}
