package io.github.dvegasa.usershowing.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.dvegasa.usershowing.R;
import io.github.dvegasa.usershowing.models.RandomUserResult;
import io.github.dvegasa.usershowing.network.RetrofitHelper;
import io.github.dvegasa.usershowing.network.RetrofitHelperContract;
import io.github.dvegasa.usershowing.screens.ModelStatusCallback;

/**
 * 11.07.2018
 */
public class RvUsersAdapter extends RecyclerView.Adapter<RvUsersAdapter.ViewHolder> {

    private List<RandomUserResult.Result> list;
    private Context context;

    public RvUsersAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<RandomUserResult.Result> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == list.size()) ? R.layout.item_button_load_more : R.layout.item_user;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == R.layout.item_button_load_more) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_button_load_more, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        if (position == list.size()){
            holder.btnLoadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    v.setVisibility(View.GONE);
                    holder.progressBar.setVisibility(View.VISIBLE);
                    final RetrofitHelperContract retrofit = new RetrofitHelper();
                    retrofit.prepare(new ModelStatusCallback() {
                        @Override
                        public void ready() {
                            retrofit.downloadRandomUsers(10, new ModelStatusCallback.onFinishedRandomUsersResult() {
                                @Override
                                public void onResponse(RandomUserResult randomUserResults) {
                                    list.addAll(randomUserResults.getResults());
                                    v.setVisibility(View.VISIBLE);
                                    holder.progressBar.setVisibility(View.GONE);
                                    notifyDataSetChanged();
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    Toast.makeText(context, "Something goes wrong", Toast.LENGTH_SHORT).show();
                                    v.setVisibility(View.VISIBLE);
                                    holder.progressBar.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            });

        } else {
            RandomUserResult.Result user = list.get(position);
            holder.tvUserEmail.setText(user.getEmail());
            holder.tvUserName.setText(user.getName().toString());

            Glide.with(context)
                    .load(user.getPicture().getThumbnail())
                    .into(holder.ivUserPicture);

            holder.cvRoot.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (!holder.isFav) {
                        int colorItemUserFav = ContextCompat.getColor(context, R.color.itemUserFav);
                        holder.cvRoot.setCardBackgroundColor(colorItemUserFav);
                        holder.isFav = true;
                    } else {
                        int colorNotFav = ContextCompat.getColor(context, R.color.itemUserNotFav);
                        holder.cvRoot.setCardBackgroundColor(colorNotFav);
                        holder.isFav = false;
                    }
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size() + 1; // +1 means button at the end of recyclerView
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Nullable @BindView(R.id.cvRoot)
        CardView cvRoot;
        @Nullable @BindView(R.id.ivUserPicture)
        ImageView ivUserPicture;
        @Nullable @BindView(R.id.tvUserName)
        TextView tvUserName;
        @Nullable @BindView(R.id.tvUserEmail)
        TextView tvUserEmail;

        @Nullable @BindView(R.id.btnLoadMore)
        Button btnLoadMore;
        @Nullable @BindView(R.id.progressBar)
        ProgressBar progressBar;

        boolean isFav = false;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
