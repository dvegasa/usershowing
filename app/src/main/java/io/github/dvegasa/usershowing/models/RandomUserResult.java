
package io.github.dvegasa.usershowing.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RandomUserResult {

    @SerializedName("results")
    @Expose
    private List<Result> results = null;
    @SerializedName("info")
    @Expose
    private Info info;

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RandomUserResult that = (RandomUserResult) o;

        if (results != null ? !results.equals(that.results) : that.results != null) return false;
        return info != null ? info.equals(that.info) : that.info == null;
    }

    @Override
    public int hashCode() {
        int result = results != null ? results.hashCode() : 0;
        result = 31 * result + (info != null ? info.hashCode() : 0);
        return result;
    }

    public static class Picture {

        @SerializedName("large")
        @Expose
        private String large;
        @SerializedName("medium")
        @Expose
        private String medium;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;

        public String getLarge() {
            return large;
        }

        public void setLarge(String large) {
            this.large = large;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Picture picture = (Picture) o;

            if (large != null ? !large.equals(picture.large) : picture.large != null) return false;
            if (medium != null ? !medium.equals(picture.medium) : picture.medium != null) return false;
            return thumbnail != null ? thumbnail.equals(picture.thumbnail) : picture.thumbnail == null;
        }

        @Override
        public int hashCode() {
            int result = large != null ? large.hashCode() : 0;
            result = 31 * result + (medium != null ? medium.hashCode() : 0);
            result = 31 * result + (thumbnail != null ? thumbnail.hashCode() : 0);
            return result;
        }
    }

    public static class Info {

        @SerializedName("seed")
        @Expose
        private String seed;
        @SerializedName("results")
        @Expose
        private int results;
        @SerializedName("page")
        @Expose
        private int page;
        @SerializedName("version")
        @Expose
        private String version;

        public String getSeed() {
            return seed;
        }

        public void setSeed(String seed) {
            this.seed = seed;
        }

        public int getResults() {
            return results;
        }

        public void setResults(int results) {
            this.results = results;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Info info = (Info) o;

            if (results != info.results) return false;
            if (page != info.page) return false;
            if (seed != null ? !seed.equals(info.seed) : info.seed != null) return false;
            return version != null ? version.equals(info.version) : info.version == null;
        }

        @Override
        public int hashCode() {
            int result = seed != null ? seed.hashCode() : 0;
            result = 31 * result + results;
            result = 31 * result + page;
            result = 31 * result + (version != null ? version.hashCode() : 0);
            return result;
        }
    }

    public static class Name {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("first")
        @Expose
        private String first;
        @SerializedName("last")
        @Expose
        private String last;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Name name = (Name) o;

            if (title != null ? !title.equals(name.title) : name.title != null) return false;
            if (first != null ? !first.equals(name.first) : name.first != null) return false;
            return last != null ? last.equals(name.last) : name.last == null;
        }

        @Override
        public int hashCode() {
            int result = title != null ? title.hashCode() : 0;
            result = 31 * result + (first != null ? first.hashCode() : 0);
            result = 31 * result + (last != null ? last.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return getFirst() + " " + getLast();
        }
    }

    public static class Result {

        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("name")
        @Expose
        private Name name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("picture")
        @Expose
        private Picture picture;

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Name getName() {
            return name;
        }

        public void setName(Name name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Picture getPicture() {
            return picture;
        }

        public void setPicture(Picture picture) {
            this.picture = picture;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Result result = (Result) o;

            if (gender != null ? !gender.equals(result.gender) : result.gender != null) return false;
            if (name != null ? !name.equals(result.name) : result.name != null) return false;
            if (email != null ? !email.equals(result.email) : result.email != null) return false;
            return picture != null ? picture.equals(result.picture) : result.picture == null;
        }

        @Override
        public int hashCode() {
            int result = gender != null ? gender.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            result = 31 * result + (email != null ? email.hashCode() : 0);
            result = 31 * result + (picture != null ? picture.hashCode() : 0);
            return result;
        }
    }
}
