package io.github.dvegasa.usershowing.network;

import io.github.dvegasa.usershowing.models.RandomUserResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * 11.07.2018
 */
public interface RandomUserClient {

    /*    |----------BASE_URL------| */
    //    https://randomuser.me/api/?inc=gender,name,email,registred,picture&results=10
    @GET("?inc=gender,name,email,registred,picture")
    Call<RandomUserResult> getRandomUsers(@Query("results") int n);
}
