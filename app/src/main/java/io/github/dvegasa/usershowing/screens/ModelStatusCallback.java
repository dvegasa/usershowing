package io.github.dvegasa.usershowing.screens;

import io.github.dvegasa.usershowing.models.RandomUserResult;

/**
 * 11.07.2018
 */
public interface ModelStatusCallback {
    public void ready();

    public interface onFinishedRandomUsersResult {
        public void onResponse(RandomUserResult randomUserResults);

        public void onFailure(Throwable t);
    }
}
