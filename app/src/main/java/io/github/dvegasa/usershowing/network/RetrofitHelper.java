package io.github.dvegasa.usershowing.network;

import io.github.dvegasa.usershowing.models.RandomUserResult;
import io.github.dvegasa.usershowing.screens.ModelStatusCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 11.07.2018
 */
public class RetrofitHelper implements RetrofitHelperContract {

    private RandomUserClient randomUserClient;

    @Override
    public void prepare(ModelStatusCallback callback) {
        randomUserClient = ServiceGenerator.createRetrofit(RandomUserClient.class);
        callback.ready();
    }

    @Override
    public void downloadRandomUsers(int n, final ModelStatusCallback.onFinishedRandomUsersResult callback) {
        Call<RandomUserResult> call = randomUserClient.getRandomUsers(n);
        call.enqueue(new Callback<RandomUserResult>() {
            @Override
            public void onResponse(Call<RandomUserResult> call, Response<RandomUserResult> response) {
                RandomUserResult randomUserResult = response.body();
                callback.onResponse(randomUserResult);
            }

            @Override
            public void onFailure(Call<RandomUserResult> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }
}
