package io.github.dvegasa.usershowing.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 11.07.2018
 */
public class ServiceGenerator {
    private static final String BASE_URL = "https://randomuser.me/api/";

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    public static <S> S createRetrofit(Class<S> serviceClass){
        return retrofit.create(serviceClass);
    }
}
