package io.github.dvegasa.usershowing.screens;

import java.util.List;

import io.github.dvegasa.usershowing.models.RandomUserResult;

/**
 * 11.07.2018
 */
public interface UsersListContract {

    public interface View {
        public void updateUsers(List<RandomUserResult.Result> userResultList);

        public void showErrorNotification(Throwable t);
    }

    public interface Presenter {
        public void viewIsReady();

        public void onDestroyView();

        public void attachView(UsersListContract.View view);
    }
}
