package io.github.dvegasa.usershowing.screens;

import io.github.dvegasa.usershowing.models.RandomUserResult;
import io.github.dvegasa.usershowing.network.RetrofitHelper;

/**
 * 11.07.2018
 */
public class UsersListPresenter implements UsersListContract.Presenter {

    private RetrofitHelper retrofitHelper;
    private UsersListContract.View view;

    private void downloadUsers() {
        retrofitHelper.downloadRandomUsers(10, new ModelStatusCallback.onFinishedRandomUsersResult() {
            @Override
            public void onResponse(RandomUserResult randomUserResults) {
                view.updateUsers(randomUserResults.getResults());
            }

            @Override
            public void onFailure(Throwable t) {
                view.showErrorNotification(t);
            }
        });
    }

    @Override
    public void viewIsReady() {
        retrofitHelper = new RetrofitHelper();
        retrofitHelper.prepare(new ModelStatusCallback() {
            @Override
            public void ready() {
                downloadUsers();
            }
        });
    }

    @Override
    public void onDestroyView() {
        view = null;
    }

    @Override
    public void attachView(UsersListContract.View view) {
        this.view = view;
    }
}
